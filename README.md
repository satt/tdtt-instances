#Thesis defense timetabling instances

This repository contains instances, solutions, MiniZinc model, and mathematical formulation of the Thesis Defence Timetabling problem defined in the paper *Thesis Defense Timetabling* by Michele Battistutta, Sara Ceschia, Fabio De Cesco, Luca Di Gaspero, and Andrea Schaerf, submitted to a journal. A [preliminary version](http://satt.diegm.uniud.it/uploads/Papers/BCDS2015.pdf) (using different instances) appeared in the proceedings of the 7th Multidisciplinary International Conference on Scheduling : Theory and Applications (MISTA 2015).

###Models

The MiniZinc model and the mathematical formulation are in the folder `Models`.


###Instances and solutions

Instances are stored in folder `Instances` and best solutions are available in the folder `Solutions`.  The following table reports for each instance, the cost of the corresponding solution.

|Instance | Cost
|:---|---:|
|tdtt-art01 | 94
|tdtt-art02 | 17
|tdtt-art03 | 8
|tdtt-art04 | 22
|tdtt-art05 | 0
|tdtt-art06 | 126
|tdtt-art07 | 248
|tdtt-art08 | 30
|tdtt-art09 | 13
|tdtt-art10 | 16
|tdtt-art11 | 244
|tdtt-art12 | 3
|tdtt-art13 | 83
|tdtt-art14 | 75
|tdtt-art15 | 201
|tdtt-art16 | 105
|tdtt-art17 | 97
|tdtt-art18 | 1
|tdtt-art19 | 2 
|tdtt-art20 | 189
|tdtt-art21 | 4
|tdtt-art22 | 14
|tdtt-art23 | 189
|tdtt-art24 | 0
|tdtt-art25 | --
|tdtt-art26 | 28
|tdtt-art27 | 299
|tdtt-art28 | 17
|tdtt-art20 | 235
|tdtt-art30 | 11
|tdtt-real01 | 59
|tdtt-real02 | 0
|tdtt-real03 | 52
|tdtt-real04 | 193
|tdtt-real05 | 520
|tdtt-real06 |46
