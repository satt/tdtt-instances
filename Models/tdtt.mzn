include "at_most.mzn";
include "at_least.mzn";
include "member.mzn";
include "disjoint.mzn";

int: Sessions;
int: FacultyMembers;
int: Candidates;
int: MaxCandidates;
int: SimultaneityPairs;
int: Levels; % 1 = Full Professor, 2 = Associate Professor, 3 = Assistant Professor, 4 = External 

int: WeightOfMultipleUse = 1;
int: WeightOfMissingOpponent = 3;

array [1..Levels,1..2] of 0..FacultyMembers: LimitMembersForLevel;
array [int,int] of -1..Sessions: SimultaneousSessions;
% if 1..SimultaneityPairs instead of int, does not work for SimultaneityPairs = 0
% for .dnz, we should use: array2d(1..SimultaneityPairs,1..2,[]); to be compatible with SimultaneityPairs = 0

array [1..FacultyMembers] of 1..Levels: AcademicLevel; 
array [1..FacultyMembers] of set of 1..Sessions: Unavailability;
array [1..Candidates] of 1..FacultyMembers: Supervisor;
array [1..Candidates] of set of 1..FacultyMembers: Opponents;

array [1..Candidates] of var 1..Sessions: StudentSession;
array [1..Sessions] of var set of 1..FacultyMembers: SessionMembers;

% redundant variables
array [1..Candidates] of var 0..FacultyMembers: NumOpponents;
var 0..Candidates: missing_opponent;
array [1..FacultyMembers] of var 0..Sessions: Presences;
var 0..1000: multiple_use;
var 0..1000: total_cost;

%% CHECKS %%%%%
constraint
  forall (s in 1..Candidates)
    (assert (not (Supervisor[s] in Opponents[s]),"Supervisor cannot be opponent"));


%%% CONSTRAINTS %%%

% number of members of each session for each level (or global)  (REDUNDANT??)
constraint
  forall (s in 1..Sessions)
    ((card(SessionMembers[s]) >= LimitMembersForLevel[4,1]) /\ 
     (card(SessionMembers[s]) <= LimitMembersForLevel[4,2]));

% all distinct members of each session: 
% ***by construction*** (due to the use of sets)

% at most MaxCandidatesPerSession students per session:
constraint
  forall (s in 1..Sessions)
    (at_most(MaxCandidates,StudentSession,s));

% supervisor present at student session
constraint
  forall (s in 1..Candidates)
     (member(SessionMembers[StudentSession[s]],Supervisor[s]));

% simultaneous sessions constraint
constraint
  forall (sim in 1..SimultaneityPairs)
   (disjoint(SessionMembers[SimultaneousSessions[sim,1]],
             SessionMembers[SimultaneousSessions[sim,2]]));

% unavailability constraint
constraint
   forall (s in 1..Sessions, p in 1..FacultyMembers)
    (if s in Unavailability[p]
     then not (p in SessionMembers[s])
     else true
     endif);

constraint 
   forall (s in 1..Sessions, lv in 1..Levels)
      (((sum (p in 1..FacultyMembers)(AcademicLevel[p] <= lv 
                                      /\ p in SessionMembers[s])) 
          >= LimitMembersForLevel[lv,1]) /\
       ((sum (p in 1..FacultyMembers)(AcademicLevel[p] <= lv  
                                      /\ p in SessionMembers[s])) 
          <= LimitMembersForLevel[lv,2]));

%%%%% COSTS (soft constraints) %%%%%%%%%%%%%%
% presence of opponents
constraint 
   forall (s in 1..Candidates)
     (NumOpponents[s] = sum (op in Opponents[s])
       (op in SessionMembers[StudentSession[s]]));

constraint 
   missing_opponent = sum (s in 1..Candidates)
        (NumOpponents[s] == 0 /\ card(Opponents[s]) > 0);

% multiple presences
constraint 
   forall (p in 1..FacultyMembers) (Presences[p] = 
         sum (s in 1..Sessions)(p in SessionMembers[s]));

constraint
    multiple_use = (sum (p in 1..FacultyMembers)((Presences[p] > 0) 
                   * (Presences[p] - 1) * (Presences[p] - 1)));

solve minimize WeightOfMultipleUse * multiple_use 
             + WeightOfMissingOpponent * missing_opponent;

output ["StudentSession = "] ++ [show(StudentSession)] ++ 
       ["\nSessionMembers = "] ++ [show(SessionMembers)] ++ 
       ["\nPresences = "] ++ [show(Presences)] ++ 
       ["\nOpponents = "] ++ [show(NumOpponents)] ++
       ["\nMultiple uses = "] ++ [show(multiple_use)] ++
       ["\nMissing opponents = "] ++ [show(missing_opponent)];